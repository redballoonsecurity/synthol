PYTHON=python3
PIP=pip3

.PHONY: develop
develop:
	$(PIP) install -e .[test]

.PHONY: install
install:
	$(PIP) install .

.PHONY: install-test
install-test:
	$(PIP) install .[test]

.PHONY: inspect
inspect:
	mypy

.PHONY: test
test: inspect
	$(PYTHON) -m pytest --cov=synthol --cov-report term-missing --junitxml=report.xml tests
