# Changelog

`synthol` adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html), and this file is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [0.1.1] - 2022-04-05
### Changed
- Pin third-party dependency `typing_inspect` to `typing_inspect~=0.7.1`.

### Fixed
- `DependencyInjector.bind`, and its wrappers `bind_factory` and `bind_instance`, previously didn't set the given provider to have the highest priority if it was already in the list of providers for its implemented interfaces.
- Fix possible errors when invalid objects were given as blacklisted modules
