import pytest

from synthol.injector import DependencyInjector
from tests import classes


@pytest.fixture(scope="session")
def injector() -> DependencyInjector:
    """Return an initialized injector with the test classes already discovered."""
    injector = DependencyInjector()
    injector.discover(classes)
    return injector
