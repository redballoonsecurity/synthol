from abc import ABC


class AInterface(ABC):
    pass


class AImpl1(AInterface):
    pass


class AImpl2(AInterface):
    pass
