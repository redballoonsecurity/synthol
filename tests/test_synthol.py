import pytest

from synthol.injector import DependencyInjector
from tests import classes
from tests.classes import AImpl1, AInterface, AImpl2


async def test_bind_priority(injector):
    """Test that the most recently bound provider has the highest priority wrt `get_instance()`."""
    for AImpl in (AImpl1, AImpl2, AImpl1, AImpl2):
        injector.bind_factory(AImpl)
        a_impl = await injector.get_instance(AInterface)
        assert isinstance(a_impl, AImpl)


def test_invalid_objects_as_blacklisted_modules():
    """
    Test that giving invalid objects as blacklisted modules doesn't make the code crash.
    """
    injector = DependencyInjector()
    invalid_modules = (
        "some string",  # not a module and no parent module
        AInterface,  # not a module but has a parent module
    )
    for invalid_module in invalid_modules:
        with pytest.raises(ValueError, match="not a valid Python module"):
            injector.discover(classes, blacklisted_modules=[invalid_module])
